# Activity:
# 1. Accept a year input from the user and determine if it is a leap year or not.
# 2. Accept two numbers (row and col) from the user and create a grid of asterisks using the two numbers (row and col).

# Stretch Goal:
# 1. Add a validation for the leap year input:
# - Strings are not allowed for inputs
# - No zero or negative values


year = int(input("Please input a year: \n"))

if type(year) == str:
    print("Please input a number")
elif year <= 0:
    print("Please input a valid year")
elif year % 4 != 0:
    print(f"{year} is not a leap year")
else:
    print(f"{year} is  a leap year")

row = int(input("Enter number of rows \n"))
column = int(input("Enter number of columns \n"))

if type(row) == str or type(column) == str:
    print("Please input a number")
elif row <= 0 or column <= 0:
    print("Invalid number of rows or columns")

i = 1
j = 1
row_value = ""

while i <= column:
    row_value += "*"
    i += 1

while j <= row:
    print(row_value)
    j+=1